import React from 'react'
import Table from './components/Table'
import HeaderDos from './components/HeaderDos';

export const Listado = () => {

    const data = [
        {
          id: 1,
          numero_documento: '1086354262',
          nombre_completo: 'Juan Andres Torres',
          departamento: 'Nariño',
          telefono: '555-1234',
          
        },
        {
          id: 2,
          numero_documento: '1086354262',
          nombre_completo: 'Juan Andres Torres',
          departamento: 'Nariño',
          telefono: '555-1234',
          
        },
        {
            id: 3,
            numero_documento: '1086354262',
            nombre_completo: 'Juan Andres Torres',
            departamento: 'Nariño',
            telefono: '555-1234',
            
          },
      ];

  return (
    <>
    <HeaderDos/>
    <div className="container mx-auto mt-4 rounded-tl">
    <h1 className="text-center text-[#609966] font-semibold text-4xl ">Listado De Productores</h1>
      <Table data={data} />
    </div>
    </>
  )
}

export default Listado
