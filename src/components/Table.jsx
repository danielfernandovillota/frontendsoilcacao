import React from 'react';
import Cultivar from '../assets/cultivar.png'
import Detalle from '../assets/detalle.png'

const Table = ({ data }) => {
  return (
    
    <table className="min-w-full divide-y divide-gray-200 mt-4">
      <thead>
        <tr>
          <th className=" bg-[#54925D] px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider rounded-tl-md">
            N° Documento
          </th>
          <th className=" bg-[#54925D] px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider">
            Nombre Completo
          </th>
          <th className=" bg-[#54925D] px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider">
            Departamento
          </th>
          <th className=" bg-[#54925D] px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider">
            Telefono
          </th>
          <th className=" bg-[#54925D] px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider rounded-tr-md">
            Detalles
          </th>
        </tr>
      </thead>
      <tbody className="bg-white divide-y divide-gray-200">
        {data.map((item, index) => (
          <tr key={item.id} className={index % 2 === 0 ? "bg-[#9DC08B]" : "bg-gray-300"}>
            <td className="  px-6 py-4 whitespace-nowrap">
              <div className="text-sm font-medium text-gray-900">{item.numero_documento}</div>
            </td>
            <td className="  px-6 py-4 whitespace-nowrap">
              <div className="text-sm font-medium text-gray-900">{item.nombre_completo}</div>
            </td>
            <td className=" px-6 py-4 whitespace-nowrap">
              <div className="text-sm font-medium text-gray-900">{item.departamento}</div>
            </td>
            <td className="  px-6 py-4 whitespace-nowrap">
              <div className="text-sm font-medium text-gray-900">{item.telefono}</div>
            </td>
            <td className=" px-6 py-4 whitespace-nowrap">
              <div className=" flex gap-x-4 text-sm text-gray-500"><img src={Detalle} className='w-10'/><img src={Cultivar} className='w-10'/></div>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
