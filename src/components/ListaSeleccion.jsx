import React from 'react'

export const ListaSeleccion = () => {
    const [selectedOption, setSelectedOption] = useState('opcion1');
  return (
    <div>
      <select
        value={selectedOption}
        onChange={(e) => setSelectedOption(e.target.value)}
      >
        <option value="opcion1">Opción 1</option>
        <option value="opcion2">Opción 2</option>
        <option value="opcion3">Opción 3</option>
      </select>
      <p>Seleccionaste: {selectedOption}</p>
    </div>
  )
}

export default ListaSeleccion
