import React from "react";
import Logo from '../assets/Logo.svg'

export const Header = () => {
  return (
    <div>
        <header className=" bg-[#54925D] py-4 items-center flex justify-between px-2">
            <img src={Logo} className='w-20  mr-10 '/>
            <h1 className="text-center text-white font-semibold text-4xl ">Información Personal y Economica</h1>
            <p className='text-white font-semibold text-4xl'>Soil</p>
        </header>
    </div>
  )
}

export default Header
